# Release history

## 0.3.0 (Unreleased)

* Allow exact matches for keys and string values in the extra metadata search
  by using double quotes.
* Improved LDAP authentication and added the option to allow LDAP users to
  change their password.
* Various smaller bug fixes and improvements.

## 0.2.0 (2020-10-02)

* Removed linking resources with groups. Group links did not add much value in
  their current form but rather lead to confusion. Something similar might be
  brought back in the future again.
* Added and improved some more translations.
* Migrate Celery configuration to new format.
* Various smaller bug fixes and improvements.

## 0.1.2 (2020-09-22)

* Added an installation script for production environments and instructions.
* Some other updates to the documentation and configuration templates.

## 0.1.1 (2020-09-15)

* Cookies are now set only for the current domain.
* Small updates to the documentation and configuration templates.

## 0.1.0 (2020-09-14)

* Data and corresponding metadata can be created, managed, linked and searched
  using records, collections and templates.
* Data can be exchanged by granting access permissions to single users or
  groups.
* Authentication is possible using the application's database, LDAP or
  Shibboleth.
* Most features can be used through both the web interface and a REST-like API.
* Most text of the application is translatable and translated into german.
* A CLI for managing the most important aspects of the application is provided.
* A basic developer documentation is provided, covering installation,
  development and API references.
* A simple plugin system is in place to easily hook into existing
  functionality, currently mostly for overriding template content.
* As an experimental feature, workflows can be created, but are not yet
  executable.
