#!/usr/bin/env python3
# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import pathlib
import shutil
import subprocess
import sys

import click


def _run_command(cmd):
    result = subprocess.run(cmd)

    if result.returncode != 0:
        sys.exit(result.returncode)


@click.command()
def build_wheel():
    """Build a Python wheel including packaged assets and compiled translations."""
    os.chdir(os.path.join(pathlib.Path(__file__).parent.absolute(), ".."))

    # Install or update the application.
    _run_command(["pip", "install", "-e", "."])

    # Build all assets and translations.
    _run_command(["kadi", "assets", "build"])
    _run_command(["kadi", "i18n", "compile"])

    # Remove all old build-related directories.
    shutil.rmtree("build", ignore_errors=True)
    shutil.rmtree("dist", ignore_errors=True)
    shutil.rmtree("kadi.egg-info", ignore_errors=True)

    # Build the wheel.
    _run_command(["python3", "setup.py", "bdist_wheel"])

    # Rebuild the assets for development.
    _run_command(["kadi", "assets", "dev"])

    click.echo("\nWheel built successfully.")


if __name__ == "__main__":
    build_wheel()
