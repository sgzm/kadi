# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

from setuptools import find_packages
from setuptools import setup


with open(os.path.join("kadi", "version.py")) as f:
    exec(f.read())


with open("README.md") as f:
    long_description = f.read()


setup(
    name="kadi",
    version=__version__,
    license="Apache-2.0",
    author="Karlsruhe Institute of Technology",
    description="Kadi4Mat, the Karlsruhe Data Infrastructure for Materials Science.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://kadi.iam-cms.kit.edu",
    project_urls={
        "Code": "https://gitlab.com/iam-cms/kadi",
        "Documentation": "https://kadi4mat.readthedocs.io/en/latest",
    },
    packages=find_packages(exclude=("tests", "tests.*")),
    include_package_data=True,
    python_requires=">=3.6",
    zip_safe=False,
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: JavaScript",
        "Programming Language :: Python :: 3",
    ],
    install_requires=[
        "celery>=4.4.7,<5.0.0",
        "chardet>=3.0.4,<4.0.0",
        "elasticsearch-dsl>=7.0.0,<8.0.0",
        "Flask>=1.1.2,<2.0.0",
        "Flask-Babel>=2.0.0,<3.0.0",
        "Flask-Limiter>=1.4.0,<2.0.0",
        "Flask-Login>=0.5.0,<1.0.0",
        "Flask-Migrate>=2.5.3,<3.0.0",
        "Flask-SQLAlchemy>=2.4.4,<3.0.0",
        "flask-talisman>=0.7.0,<1.0.0",
        "Flask-WTF>=0.14.3,<0.15.0",
        "ldap3>=2.8.1,<3.0.0",
        "Markdown>=3.2.2,<4.0.0",
        "marshmallow>=3.8.0,<4.0.0",
        "Pillow>=7.2.0,<8.0.0",
        "pluggy>=0.13.1,<1.0.0",
        "psycopg2-binary>=2.8.6,<3.0.0",
        "PyJWT>=1.7.1,<2.0.0",
        "python-magic>=0.4.15,<1.0.0",
        "redis>=3.5.3,<4.0.0",
        "sentry-sdk[flask]>=0.18.0,<1.0.0",
        "Werkzeug[watchdog]>=1.0.1,<2.0.0",
        "WTForms[email]>=2.3.3,<2.4.0",
    ],
    extras_require={
        "dev": [
            "black==20.8b1",
            "pre-commit>=2.7.1",
            "pylint>=2.4.4,<2.5.0",
            "pytest>=6.1.1",
            "pytest-cov>=2.10.1",
            "python-dotenv>=0.14.0",
            "tox>=3.20.0",
        ],
        "docs": [
            "Sphinx>=3.2.1",
            "sphinx-rtd-theme>=0.5.0",
            "sphinxcontrib-httpdomain>=1.7.0",
        ],
    },
    entry_points={"console_scripts": ["kadi=kadi.cli.main:kadi"]},
)
