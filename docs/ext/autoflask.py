# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import itertools
import re
from collections import OrderedDict

from docutils.parsers.rst import directives
from marshmallow import fields
from sphinxcontrib.autohttp.common import import_object
from sphinxcontrib.autohttp.flask import AutoflaskDirective
from sphinxcontrib.autohttp.flask_base import AutoflaskBase as _AutoflaskBase
from sphinxcontrib.autohttp.flask_base import get_routes
from sphinxcontrib.autohttp.flask_base import prepare_docstring
from werkzeug.http import HTTP_STATUS_CODES
from wtforms import FileField
from wtforms import IntegerField
from wtforms import StringField

from kadi.config import BaseConfig
from kadi.lib.schemas import KadiSchema
from kadi.lib.schemas import NonEmptyString


# The following codes are slightly modified versions of httpdomain's "autoflask"
# directive and helper functions, which are available at the following URL:
# https://github.com/sphinx-contrib/httpdomain/tree/1.7/sphinxcontrib/autohttp
#
# httpdomain is licensed under the two-clause BSD license:
#
# Copyright (c) 2010 by the contributors (see AUTHORS file).
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


def _reqbody_field(name, meta, indent=4):
    line = f"{indent * ' '}* **{name}**"
    if meta.get("required", False):
        line += " (*required*)"

    if meta.get("many", False):
        line += f" - [{meta['type']}]"
    else:
        line += f" - {meta['type']}"

    yield line


def _reqschema(fields, indent=4):
    for name, reqschema_meta in fields.items():
        yield from _reqbody_field(name, reqschema_meta, indent=indent)

        if "nested" in reqschema_meta:
            yield ""
            yield from _reqschema(reqschema_meta["nested"], indent + 2)
            yield ""


def _get_reqschema_fields(schema):
    # We only care about the types currently in use and just use "object" as fallback.
    type_mapping = {
        NonEmptyString: "string",
        fields.String: "string",
        fields.Integer: "integer",
    }

    fields_meta = OrderedDict()

    for name, field in schema.fields.items():
        if not field.dump_only:
            field_meta = {
                "type": type_mapping.get(field.__class__, "object"),
                "required": field.required,
            }

            if schema.partial == True or (
                isinstance(schema.partial, tuple) and name in schema.partial
            ):
                field_meta["required"] = False

            if isinstance(field, fields.Pluck):
                field_meta["many"] = field.many
                field_meta["type"] = type_mapping.get(
                    field.schema.fields[field.field_name].__class__, "object"
                )

            elif isinstance(field, fields.Nested):
                field_meta["many"] = field.many
                field_meta["nested"] = _get_reqschema_fields(field.schema)

            fields_meta[name] = field_meta

    sorted_by_name = sorted(fields_meta.items(), key=lambda field: field[0])

    return OrderedDict(
        sorted(sorted_by_name, key=lambda field: field[1]["required"], reverse=True)
    )


def http_directive(method, paths, docstring, apidoc_meta):
    # Method and endpoint
    method = method.lower().strip()
    paths = [paths] if isinstance(paths, str) else paths

    for path in paths:
        yield f".. http:{method}:: {path}"

    # Docstring
    if isinstance(docstring, str):
        docstring = docstring.splitlines()

    yield ""
    for line in docstring:
        yield f"  {line}"

    # Versions
    yield ""
    versions = apidoc_meta.get("versions", BaseConfig.API_VERSIONS)
    versions = [
        f"``{v}``" + " (*latest*)" if v == BaseConfig.API_VERSIONS[-1] else str(v)
        for v in versions
    ]
    versions.reverse()
    yield f"  *Versions:* {', '.join(versions)}"

    # Required scopes
    if "scopes_required" in apidoc_meta:
        scopes = apidoc_meta["scopes_required"]["scopes"]
        operator = apidoc_meta["scopes_required"]["operator"]
        required_scopes = f"`` *{operator}* ``".join(scopes)

        yield ""
        yield f"  *Required scopes:* ``{required_scopes}``"

    # Query parameters
    if "qparams" in apidoc_meta or "pagination" in apidoc_meta:
        yield ""
        yield "  **Query parameters**"
        yield ""

        # Pagination
        pagination_meta = apidoc_meta.get("pagination")
        if pagination_meta:
            page = "    * **page** - The current result page."
            if pagination_meta["page_max"]:
                page += f" Limited to a maximum of ``{pagination_meta['page_max']}``."

            page += " (*default:* ``1``)"
            yield page

            per_page = "    * **per_page** - Number of results per page."
            if pagination_meta["per_page_max"]:
                per_page += (
                    f" Limited to a maximum of ``{pagination_meta['per_page_max']}``."
                )

            per_page += " (*default:* ``10``)"
            yield per_page

        # Others
        for name, qparam_meta in apidoc_meta.get("qparams", {}).items():
            default = qparam_meta["default"]
            if isinstance(default, str):
                default = f"'{default}'"

            qparam = f"    * **{name}** - {qparam_meta['description']}"

            if qparam_meta["multiple"]:
                qparam += " Can be specified more than once."

            qparam += f" (*default:* ``{default}``)"
            yield qparam

    # Request body, either given directly or via schema
    if "reqbody" in apidoc_meta or "reqschema" in apidoc_meta:
        reqbody_meta = apidoc_meta.get("reqbody") or apidoc_meta["reqschema"]
        reqbody_types = {"form": "form data", "json": "JSON object"}

        yield ""
        yield f"  **Request {reqbody_types[reqbody_meta.get('type', 'json')]}**"
        yield ""

        if reqbody_meta["description"]:
            yield f"    {reqbody_meta['description']}"
            yield ""

        if "reqbody" in apidoc_meta:
            for name, meta in reqbody_meta["data"].items():
                yield from _reqbody_field(name, meta)
        else:
            fields = _get_reqschema_fields(reqbody_meta["schema"])
            yield from _reqschema(fields)

    # Status codes
    if "status_codes" in apidoc_meta:
        yield ""
        yield "  **Status codes**"
        yield ""

        for status, description in apidoc_meta["status_codes"].items():
            yield (
                f"    * **{status}** (*{HTTP_STATUS_CODES.get(status, 'Unknown')}*)"
                f" - {description}"
            )

    yield ""


def quickref_directive(method, path, docstring):
    method = method.lower().strip()
    if isinstance(docstring, str):
        docstring = docstring.splitlines()

    if len(docstring) > 0:
        description = docstring[0]
    else:
        description = ""

    ref = path.replace("<", "(").replace(">", ")").replace("/", "-").replace(":", "-")

    yield f"    * - `{method.upper()} {path} <#{method.lower()}-{ref}>`_"
    yield f"      - {description}"


class AutoflaskBase(_AutoflaskBase):
    @property
    def version(self):
        return self.options.get("version", None)

    @property
    def package(self):
        return self.options.get("package", None)

    @property
    def methods(self):
        methods = self.options.get("methods", None)
        if not methods:
            return None
        return [m.lower() for m in re.split(r"\s*,\s*", methods)]

    def get_routes_iter(self, app):
        routes = self.inspect_routes(app)
        if "view" in self.groupby:
            routes = self.groupby_view(routes)

        return routes

    def make_rst(self, **kwargs):
        app = import_object(self.arguments[0])

        autoquickref = self.options.get("autoquickref", False) is None
        if autoquickref:
            yield ""
            yield ".. list-table::"
            yield "    :class: narrow-table"
            yield ""

            routes_iter = self.get_routes_iter(app)
            for method, paths, view_func, view_doc in routes_iter:
                docstring = prepare_docstring(view_doc)

                for path in paths:
                    yield from quickref_directive(method, path, docstring)

        routes_iter = self.get_routes_iter(app)
        for method, paths, view_func, view_doc in routes_iter:
            docstring = prepare_docstring(view_doc)
            apidoc_meta = getattr(view_func, "_apidoc", {})

            yield from http_directive(method, paths, docstring, apidoc_meta)

    def inspect_routes(self, app):
        order = self.order or "path"
        if self.endpoints:
            routes = itertools.chain(
                *[get_routes(app, endpoint, order) for endpoint in self.endpoints]
            )
        else:
            routes = get_routes(app, order=order)

        for method, paths, endpoint in routes:
            try:
                blueprint, _, _ = endpoint.rpartition(".")
                if self.blueprints and blueprint not in self.blueprints:
                    continue
                if blueprint in self.undoc_blueprints:
                    continue
            except ValueError:
                pass

            if endpoint in self.undoc_endpoints:
                continue

            if endpoint == "static":
                continue

            view = app.view_functions[endpoint]

            if self.modules and view.__module__ not in self.modules:
                continue

            if self.undoc_modules and view.__module__ in self.modules:
                continue

            # Filter out endpoints not being in the specificed package.
            if self.package and not view.__module__.startswith(self.package):
                continue

            # Filter out endpoints not having the correct HTTP method.
            if self.methods and method.lower() not in self.methods:
                continue

            view_class = getattr(view, "view_class", None)
            if view_class is None:
                view_func = view
            else:
                view_func = getattr(view_class, method.lower(), None)

            # Filter out internal endpoints.
            if getattr(view_func, "_internal", False):
                continue

            # Filter out endpoints not allowing token access.
            scopes_meta = getattr(view_func, "_scope", None)
            if scopes_meta is not None and len(scopes_meta["scopes"]) == 0:
                continue

            # Filter out endpoints not belonging to the given API version.
            if self.version:
                if not re.match(f"^.*_v{self.version.replace('.', '_')}$", endpoint):
                    continue
            # Otherwise skip all versioned endpoints.
            else:
                if re.match("^.*_v[0-9]+_[0-9]+$", endpoint):
                    continue

            view_doc = view.__doc__ or ""
            if view_func and view_func.__doc__:
                view_doc = view_func.__doc__

            if not view_doc and "include-empty-docstring" not in self.options:
                continue

            yield (method, paths, view_func, view_doc)


class Autoflask(AutoflaskBase, AutoflaskDirective):
    """Modified ``autoflask`` directive.

    Has some modified output, default values and filters and additionaly provides the
    following options:

    * ``:version:`` Specify a single API version that should be documented (based on the
      associated endpoints). If the option is not provided, all unversioned endpoints
      (which automatically point to the latest version) will be taken instead.
    * ``:package:`` Limits the documented endpoints to those in the specified package
      and its subpackages.
    * ``:methods:`` Limits the documented endpoints by specified HTTP methods. Multiple
      methods can be given separated by commas.

    **Example:**

    .. code-block:: rst

       :version: 1.0
       :package: kadi.modules.records.api
       :methods: get, post
    """


Autoflask.option_spec["version"] = directives.unchanged
Autoflask.option_spec["package"] = directives.unchanged
Autoflask.option_spec["methods"] = directives.unchanged


def setup(app):
    app.add_directive("autoflask", Autoflask)
