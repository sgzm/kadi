Testing
=======

This section describes different aspects about testing the application code.

Tests are currently focused on the backend (i.e. Python) code and can be found
in the :file:`tests` directory. For writing and running tests, `pytest
<https://docs.pytest.org/en/latest/>`__ is used, in combination with `tox
<https://tox.readthedocs.io/en/latest/>`__.

Setting up the environment
--------------------------

For testing code locally, a separate database needs to be created. The setup is
similar to before (see also :ref:`Configuring Postgres
<installation-development-configuration-postgres>`):

.. code-block:: bash

  sudo -u postgres createuser -P kadi_test
  sudo -u postgres createdb -O kadi_test kadi_test -E utf-8

When prompted for a password, it is best to use ``kadi_test`` (again so the
default test configuration can be used).

Running tests
--------------

Pytest should discover all tests automatically when run inside the project's
root directory:

.. code-block:: bash

  pytest

This will run all tests using the current local environment. For running the
complete test suite that is also run as part of the CI pipeline, tox is used
instead:

.. code-block:: bash

  tox

This may take some time to run since it includes some other commands besides
pytest and also may run for multiple Python environments and dependencies. If
any Python version is missing locally, the test suite will emit a warning and
the tests will be skipped for that version.
