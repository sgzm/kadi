Development
===========

This chapter covers various information about developing or contributing to
Kadi4Mat.

.. toctree::
   :maxdepth: 2

   General <general>
   Style guide <style_guide>
   Testing <testing>
   Plugins <plugins>
   Translations <translations>
   Documentation <documentation>
   Todos <todos>
