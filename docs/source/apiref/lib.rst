.. _apiref-lib:

Lib
===

This section contains all API references of the :mod:`kadi.lib` package that
were not already listed in other sections.

API
---

.. automodule:: kadi.lib.api.blueprint
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.api.core
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.api.schemas
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.api.utils
   :members:
   :show-inheritance:

Archives
--------

.. automodule:: kadi.lib.archives
   :members:
   :show-inheritance:

Cache
-----

.. automodule:: kadi.lib.cache
   :members:
   :show-inheritance:

Conversion
----------

.. automodule:: kadi.lib.conversion
   :members:
   :show-inheritance:

Database
--------

.. automodule:: kadi.lib.db
   :members:
   :show-inheritance:

Exceptions
----------

.. automodule:: kadi.lib.exceptions
   :members:
   :show-inheritance:

Format
------

.. automodule:: kadi.lib.format
   :members:
   :show-inheritance:

Forms
-----

.. automodule:: kadi.lib.forms
   :members:
   :show-inheritance:

Jinja
-----

.. automodule:: kadi.lib.jinja
   :members:
   :show-inheritance:

JWT
---

.. automodule:: kadi.lib.jwt
   :members:
   :show-inheritance:

LDAP
----

.. automodule:: kadi.lib.ldap
   :members:
   :show-inheritance:

Mails
-----

.. automodule:: kadi.lib.mails.core
   :members:
   :show-inheritance:

Resources
---------

.. automodule:: kadi.lib.resources.api
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.resources.utils
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.resources.views
   :members:
   :show-inheritance:

Revisions
---------

.. automodule:: kadi.lib.revisions.core
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.revisions.schemas
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.revisions.utils
   :members:
   :show-inheritance:

Schemas
-------

.. automodule:: kadi.lib.schemas
   :members:
   :show-inheritance:

Search
------

.. automodule:: kadi.lib.search.core
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.search.elasticsearch
   :members:
   :show-inheritance:

Storage
-------

.. automodule:: kadi.lib.storage.core
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.storage.local
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.storage.misc
   :members:
   :show-inheritance:

Tags
----

.. automodule:: kadi.lib.tags.core
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.tags.schemas
   :members:
   :show-inheritance:

Tasks
-----

.. automodule:: kadi.lib.tasks.core
   :members:
   :show-inheritance:

.. automodule:: kadi.lib.tasks.schemas
   :members:
   :show-inheritance:

Utils
-----

.. automodule:: kadi.lib.utils
   :members:
   :show-inheritance:

Validation
----------

.. automodule:: kadi.lib.validation
   :members:
   :show-inheritance:

Web
---

.. automodule:: kadi.lib.web
   :members:
   :show-inheritance:
