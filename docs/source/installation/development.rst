.. _installation-development:

Development
===========

This section describes how to get a complete working environment running for
development and testing.

.. note::

  This section uses a special directory structure in its examples, which should
  make it very easy to follow along with all the steps. However, this structure
  can of course be changed if it is not desirable, as in practice a different
  structure may be more convenient.

.. include:: dependencies.rst

Node.js
~~~~~~~

`Node.js <https://nodejs.org/en/>`__, including `npm
<https://www.npmjs.com/>`__, are used for managing the frontend dependencies
and building the assets bundles. They can be installed using:

.. code-block:: bash

  sudo apt install curl
  curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
  sudo apt install nodejs

Installing Kadi4Mat
-------------------

The first step to installing Kadi4Mat is to get the source code using `git
<https://git-scm.com>`__. For this, it is recommended to create a fork of the
`main repository <https://gitlab.com/iam-cms/kadi>`__ first. Afterwards, the
code can be cloned into a local directory via SSH or HTTPS, although SSH is
generally recommended. Note that the *<username>* placeholder needs to be
substituted with the correct username/namespace that the new fork sits in:

.. code-block:: bash

  mkdir ${HOME}/kadi
  git clone git@gitlab.com:<username>/kadi.git ${HOME}/kadi/src

This will copy the code into the ``kadi/src`` directory in the current user's
home directory. To be able to get code changes from the central repository, it
should be added as an additional remote called *upstream* for example (note
that the default remote after cloning, pointing to the new fork, is always
called *origin*):

.. code-block:: bash

  cd ${HOME}/kadi/src
  git remote add upstream git@gitlab.com:iam-cms/kadi.git

To create and activate a new virtual environment for the application, the
following commands can be used:

.. code-block:: bash

  virtualenv -p python3 ${HOME}/kadi/venv
  source ${HOME}/kadi/venv/bin/activate

This will create and activate a new virtual environment named *venv* using
Python 3 as interpreter. For all following steps the virtual environment is
assumed to be active.

To install the application, the following command can be used:

.. code-block:: bash

  pip install -e ${HOME}/kadi/src[dev]

This will install the application in editable mode, which simply creates a link
to the sources so all changes are reflected in the installed package
immediately. Using *[dev]* also installs all development dependencies listed in
:file:`setup.py`.

At this point, it is also recommended to already install the pre-commit hooks
by running:

.. code-block:: bash

  pre-commit install

For more information please refer to :ref:`Tools <development-general-tools>`.

Configuration
-------------

.. _installation-development-configuration-postgres:

Postgres
~~~~~~~~

To set up Postgres, a user and a database belonging to that user have to be
created:

.. code-block:: bash

  sudo -u postgres createuser -P kadi
  sudo -u postgres createdb -O kadi kadi -E utf-8

When prompted for a password, use ``kadi``. This way, the default configuration
of the application does not need to be changed later on.

.. _installation-development-configuration-kadi4mat:

Kadi4Mat
~~~~~~~~

Some environment variables need to be set in order to use the application and
the correct configuration. Those can be either set directly on the command line
each time or in a :file:`.env` file (which needs to be created first),
preferably residing in the project's root directory (meaning
``${HOME}/kadi/src`` when following along with the example directory
structure). More details about this can be found in the `Flask Documentation
<https://flask.palletsprojects.com/en/1.1.x/cli/#environment-variables-from
-dotenv>`__.

When using the command line, the following can be used to specify how to load
the application and to set the correct environment for development:

.. code-block:: bash

  export FLASK_APP=${HOME}/kadi/src/kadi/wsgi.py
  export FLASK_ENV=development

Alternatively when using the :file:`.env` file instead:

.. code-block:: none

  FLASK_APP=${HOME}/kadi/src/kadi/wsgi.py
  FLASK_ENV=development

For the development environment all configuration values have default values
set which correspond to the configuration values used in this documentation, so
no further changes should be necessary. However, if any of those values need to
be changed regardless, it is best done using the ``KADI_CONFIG_FILE``
environment variable. This variable needs to point to a valid configuration
Python file (which also needs to be created first) in which the desired values
can be specified. This file can be treated like a normal Python file, i.e.
calculating values or importing other modules will work, however, the
syntactical rules and formatting of Python also apply. For example, the
following can be used on the command line assuming the file
:file:`config/development.py` is our configuration file:

.. code-block:: bash

  export KADI_CONFIG_FILE=${HOME}/kadi/config/development.py

Similarly, the same can again be specified in the :file:`.env` file instead.
See how to :ref:`configure Kadi4Mat in a production environment
<installation-production-configuration-kadi4mat>` for an example of how some
configuration values can be overridden.

Setting up the application
--------------------------

Before the application can be used, some initialization steps have to be done
using the Kadi command line interface (CLI):

.. code-block:: bash

  kadi assets dev   # Install the frontend dependencies and build the static assets
  kadi db init      # Initialize the database
  kadi search init  # Initialize the search indices
  kadi i18n compile # Compile the backend translations

The Kadi CLI offers some useful tools and utility functions running in the
context of the application (see also :ref:`Command line interfaces
<development-general-cli>`). As such, it also needs access to the correct
configuration specified by the environment variables explained in the previous
section. When using the :file:`.env` file instead, this means that the commands
have to be run inside the directory that contains this file (or in any child
directory, actually).

Another useful command when setting up the appliction for the first time is the
following one, which can be used to set up some initial local dummy users and
resources:

.. code-block:: bash

  kadi db test-data

The three main dummy users useful for testing are:

========   =========   =================================
Username   Password    System Role
========   =========   =================================
admin      admin123    Admin  (Can modify any resource)
member     member123   Member (Can create new resources)
guest      guest123    Guest  (Read only access)
========   =========   =================================

Running the application
-----------------------

Flask includes a lightweight development server, which can be run using the
Flask CLI (see also :ref:`Command line interfaces <development-general-cli>`):

.. code-block:: bash

  flask run

When using the :file:`.env` file and the application cannot be found, make sure
to run the command in the correct directory. Afterwards the application should
run locally on port 5000:

.. code-block:: bash

  firefox http://localhost:5000

To be able to run asynchronous background tasks with Celery (needed for example
when uploading files or sending emails), the following command can be used:

.. code-block:: bash

  kadi celery worker -B --loglevel=INFO

This will start the normal Celery worker as well as Celery beat, which is used
for periodic tasks (e.g. for deleting expired uploads) in a single process,
which is convenient for development.

To "send" emails without using an actual SMTP server, the following can be used
to simply print the emails on the terminal instead (this debugging server is
already configured as default when developing):

.. code-block:: bash

  python -m smtpd -n -c DebuggingServer localhost:8025

Updating the application
------------------------

After updating the application code, the following commands may have to be run
again:

.. code-block:: bash

  pip install -e ${HOME}/kadi/src[dev] # Install any new backend dependencies
  kadi assets dev                      # Install any new frontend dependencies and rebuild the static assets
  kadi db upgrade                      # Upgrade the database schema
  kadi i18n compile                    # Recompile any new backend translations

In case there are changes in the search mappings, it may also be necessary to
rebuild the search index:

.. code-block:: bash

  kadi search reindex

The command will create a new index using the updated search mappings and also
repopulate it. Until then, the old search index should still be usable.

Note that any plugins that might have changed will need to be updated
separately. Furthermore, configuration options or templates may have been
changed, which need to be updated manually in any configuration file.
