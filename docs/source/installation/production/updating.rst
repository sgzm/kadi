Updating the application
========================

When updating Kadi4Mat, it is recommended to first stop the Celery services and
afterwards the web and application server:

.. code-block:: bash

  sudo systemctl stop kadi-celery kadi-celerybeat
  sudo systemctl stop apache2 uwsgi

Afterwards, the application can be updated:

.. code-block:: bash

  sudo su - kadi      # Switch to the kadi user
  pip install kadi -U # Update the application code
  kadi db upgrade     # Upgrade the database schema

In case there are changes in the search mappings, it may also be necessary to
rebuild the search index:

.. code-block:: bash

  sudo su - kadi
  kadi search reindex --force

The ``--force`` flag is needed when working inside a production environment.
The command will create a new index using the updated search mappings and also
repopulate it. Until then, the old search index should still be usable.

Note that any plugins that might have changed will need to be updated
separately. Furthermore, configuration options or templates may have been
changed, which need to be updated manually in any configuration file.

Finally, all services can be started again:

.. code-block:: bash

  sudo systemctl start apache2 uwsgi kadi-celery kadi-celerybeat
