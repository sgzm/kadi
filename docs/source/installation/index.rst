Installation
============

This chapter covers installing Kadi4Mat either for :doc:`Development
<development>` or :doc:`Production <production>`. The installation has
currently been tested under Debian 10 (Buster), which the installation
instructions are also based on. However, other current Debian-based
distributions like Ubuntu (>= 18) should work as well.

.. toctree::
   :maxdepth: 2

   Development <development>
   Production <production>
