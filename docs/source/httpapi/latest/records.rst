Records
=======

POST
----

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.records.api
   :methods: post
   :autoquickref:

GET
---

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.records.api
   :methods: get
   :autoquickref:

PATCH
-----

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.records.api
   :methods: patch
   :autoquickref:

PUT
---

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.records.api
   :methods: put
   :autoquickref:

DELETE
------

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.records.api
   :methods: delete
   :autoquickref:
