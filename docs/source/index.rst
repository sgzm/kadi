.. image:: /images/kadi.png
   :width: 150 px
   :align: right

Welcome to Kadi4Mat's documentation!
====================================

**Kadi4Mat** is the **Karlsruhe Data Infrastructure for Materials Science**, a
software for managing research data with the aim of combining new concepts with
established technologies and existing solutions.

Kadi4Mat is being developed as part of several research projects at the
`Institute for Applied Materials - Computational Materials Science (IAM-CMS)
<https://www.kit.edu/english/index.php>`__ of the `Karlsruhe Institute of
Technology (KIT) <https://www.iam.kit.edu/cms/english/index.php>`__.

.. note::

  This documentation is still a work in progress and does not cover all aspects
  of Kadi4Mat yet.

Table of Contents
=================

.. toctree::
   :maxdepth: 2

   Installation <installation/index>
   Development <development/index>
   API reference <apiref/index>
   HTTP API <httpapi/index>
