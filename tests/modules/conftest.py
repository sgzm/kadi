# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest


@pytest.fixture(autouse=True)
def _send_mail_task(monkeypatch):
    """Fixture to patch the task "kadi.notifications.send_mail".

    Will simulate the code the actual Celery task would run. The function to lanch the
    task will be patched in all relevant modules where it is used.

    Executed automatically for each test.
    """

    def _start_send_mail_task(*args):
        return True

    monkeypatch.setattr(
        "kadi.modules.accounts.views.send_email_confirmation_mail",
        _start_send_mail_task,
    )
    monkeypatch.setattr(
        "kadi.modules.settings.views.send_email_confirmation_mail",
        _start_send_mail_task,
    )
    monkeypatch.setattr(
        "kadi.modules.accounts.views.send_password_reset_mail", _start_send_mail_task
    )
