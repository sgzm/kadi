# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from kadi.modules.collections.core import delete_collection
from kadi.modules.collections.models import Collection
from tests.modules.resources import check_api_post_subject_resource_role
from tests.utils import check_api_response


def test_restore_collection(client, db, dummy_collection, user_session):
    """Test the internal "api.restore_collection" endpoint."""
    with user_session():
        delete_collection(dummy_collection)
        response = client.post(
            url_for("api.restore_collection", id=dummy_collection.id)
        )

        check_api_response(response)
        assert dummy_collection.state == "active"


def test_purge_collection(client, db, dummy_collection, user_session):
    """Test the internal "api.purge_collection" endpoint."""
    with user_session():
        delete_collection(dummy_collection)
        response = client.post(url_for("api.purge_collection", id=dummy_collection.id))

        check_api_response(response, status_code=204)
        assert Collection.query.get(dummy_collection.id) is None


def test_new_collection(api_client, dummy_access_token):
    """Test the "api.new_collection" endpoint."""
    response = api_client(dummy_access_token).post(
        url_for("api.new_collection"), json={"identifier": "test", "title": "test"}
    )

    check_api_response(response, status_code=201)
    assert Collection.query.filter_by(identifier="test").first() is not None


def test_add_collection_record(
    api_client, dummy_access_token, dummy_collection, dummy_record
):
    """Test the "api.add_collection_record" endpoint."""
    response = api_client(dummy_access_token).post(
        url_for("api.add_collection_record", id=dummy_collection.id),
        json={"id": dummy_record.id},
    )

    check_api_response(response, status_code=201)
    assert dummy_collection.records[0].id == dummy_record.id


def test_add_collection_user_role(
    api_client, dummy_access_token, dummy_collection, new_user
):
    """Test the "api.add_collection_user_role" endpoint."""
    check_api_post_subject_resource_role(
        api_client(dummy_access_token),
        url_for("api.add_collection_user_role", id=dummy_collection.id),
        new_user(),
        dummy_collection,
    )


def test_add_collection_group_role(
    api_client, dummy_access_token, dummy_collection, dummy_group
):
    """Test the "api.add_collection_group_role" endpoint."""
    check_api_post_subject_resource_role(
        api_client(dummy_access_token),
        url_for("api.add_collection_group_role", id=dummy_collection.id),
        dummy_group,
        dummy_collection,
    )
