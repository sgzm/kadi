# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.resources.utils import add_link
from kadi.lib.web import url_for
from kadi.modules.collections.models import Collection
from tests.utils import check_view_response


def test_collections(client, user_session):
    """Test the "collections.collections" endpoint."""
    with user_session():
        response = client.get(url_for("collections.collections"))
        check_view_response(response)


def test_new_collection(client, user_session):
    """Test the "collections.new_collection" endpoint."""
    endpoint = url_for("collections.new_collection")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={"identifier": "test", "title": "test", "visibility": "private"},
        )

        check_view_response(response, status_code=302)
        assert Collection.query.filter_by(identifier="test").first() is not None


def test_edit_collection(client, dummy_collection, user_session):
    """Test the "collections.edit_collection" endpoint."""
    endpoint = url_for("collections.edit_collection", id=dummy_collection.id)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"identifier": "test"})

        check_view_response(response, status_code=302)
        assert dummy_collection.identifier == "test"


def test_view_collection(client, dummy_collection, user_session):
    """Test the "collections.view_collection" endpoint."""
    with user_session():
        response = client.get(
            url_for("collections.view_collection", id=dummy_collection.id)
        )
        check_view_response(response)


@pytest.mark.parametrize("export_type", ["json"])
def test_export_collection(export_type, client, dummy_collection, user_session):
    """Test the "collections.export_collection" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "collections.export_collection",
                id=dummy_collection.id,
                export_type=export_type,
            )
        )
        check_view_response(response)


def test_manage_links(
    client, dummy_collection, dummy_group, dummy_record, new_record, user_session
):
    """Test the "collections.manage_links" endpoint."""
    endpoint = url_for("collections.manage_links", id=dummy_collection.id)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"records": [dummy_record.id]})

        check_view_response(response)
        assert dummy_collection.records[0].id == dummy_record.id


def test_manage_permissions_collection(
    client, dummy_collection, dummy_group, new_user, user_session
):
    """Test the "collection" tab of the "collections.manage_permissions" endpoint."""
    endpoint = url_for(
        "collections.manage_permissions", id=dummy_collection.id, tab="collection"
    )
    new_role = "member"
    user = new_user()

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={"users": [user.id], "groups": [dummy_group.id], "role": new_role},
        )

        check_view_response(response)
        assert (
            user.roles.filter_by(
                object="collection", object_id=dummy_collection.id, name=new_role
            ).first()
            is not None
        )
        assert (
            dummy_group.roles.filter_by(
                object="collection", object_id=dummy_collection.id, name=new_role
            ).first()
            is not None
        )


def test_manage_permissions_records(
    client,
    dummy_collection,
    dummy_group,
    dummy_record,
    dummy_user,
    new_record,
    new_user,
    user_session,
):
    """Test the "records" tab of the "collections.manage_permissions" endpoint."""
    endpoint = url_for(
        "collections.manage_permissions", id=dummy_collection.id, tab="records"
    )
    new_role = "member"
    user = new_user()

    add_link(dummy_collection.records, dummy_record, user=dummy_user)

    # Append that link directly, without checking permissions, so we can test if
    # permissions of that record are actually not granted.
    record = new_record(creator=new_user())
    dummy_collection.records.append(record)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        # Add a role.
        response = client.post(
            endpoint,
            data={"users": [user.id], "groups": [dummy_group.id], "role": new_role},
        )

        check_view_response(response)
        assert (
            user.roles.filter_by(
                object="record", object_id=dummy_record.id, name=new_role
            ).first()
            is not None
        )
        assert (
            user.roles.filter_by(object="record", object_id=record.id).first() is None
        )
        assert (
            dummy_group.roles.filter_by(
                object="record", object_id=dummy_record.id, name=new_role
            ).first()
            is not None
        )
        assert (
            dummy_group.roles.filter_by(object="record", object_id=record.id).first()
            is None
        )

        # Remove the role again.
        response = client.post(
            endpoint, data={"users": [user.id], "groups": [dummy_group.id], "role": ""}
        )

        check_view_response(response)
        assert (
            user.roles.filter_by(object="record", object_id=dummy_record.id).first()
            is None
        )
        assert (
            dummy_group.roles.filter_by(
                object="record", object_id=dummy_record.id
            ).first()
            is None
        )


def test_view_revision(client, dummy_collection, user_session):
    """Test the "collections.view_revision" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "collections.view_revision",
                collection_id=dummy_collection.id,
                revision_id=dummy_collection.revisions[0].id,
            )
        )
        check_view_response(response)


def test_delete_collection(client, dummy_collection, user_session):
    """Test the "collections.delete_collection" endpoint."""
    with user_session():
        response = client.post(
            url_for("collections.delete_collection", id=dummy_collection.id)
        )

        check_view_response(response, status_code=302)
        assert dummy_collection.state == "deleted"
