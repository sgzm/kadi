# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app
from flask_login import current_user


def test_user_email_confirmed(dummy_user):
    """Test if the "email_confirmed" property of users works correctly."""
    assert not dummy_user.email_confirmed

    dummy_user.identity.email_confirmed = True

    assert dummy_user.email_confirmed


def test_user_needs_email_confirmation(monkeypatch, dummy_user, new_user):
    """Test if the "needs_email_confirmation" property of users works correctly."""
    assert not dummy_user.needs_email_confirmation

    monkeypatch.setitem(
        current_app.config["AUTH_PROVIDERS"]["local"],
        "email_confirmation_required",
        True,
    )

    assert dummy_user.needs_email_confirmation

    dummy_user.identity.email_confirmed = True

    assert not dummy_user.needs_email_confirmation


def test_user_is_active(monkeypatch, request_context, user_session):
    """Test if the "is_active" property of users works correctly."""
    assert not current_user.is_active

    with user_session():
        assert current_user.is_active

        monkeypatch.setitem(
            current_app.config["AUTH_PROVIDERS"]["local"],
            "email_confirmation_required",
            True,
        )

        assert not current_user.is_active

        current_user.identity.email_confirmed = True

        assert current_user.is_active


def test_user_merge(dummy_record, dummy_user, new_user):
    """Test if merging multiple users works correctly."""
    user = new_user()
    user.merge(dummy_user)

    assert dummy_user.is_merged
    assert not dummy_user.identities.all()
    assert not dummy_user.records.all()
    # Only the system role should remain.
    assert not dummy_user.roles.count() == 1

    assert not user.is_merged
    assert not dummy_user.identities.count() == 2
    assert user.records.first().id == dummy_record.id
    assert (
        user.roles.filter_by(object="record", object_id=dummy_record.id).first()
        is not None
    )
