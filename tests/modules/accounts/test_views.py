# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app
from flask import get_flashed_messages
from flask_login import current_user

from kadi.lib.web import url_for
from kadi.modules.accounts.models import LocalIdentity
from tests.utils import check_view_response


def test_login(client):
    """Test the "accounts.login" endpoint."""
    response = client.get(url_for("accounts.login"))
    check_view_response(response)


def test_login_with_provider_local(client, dummy_user, request_context):
    """Test the "accounts.login_with_provider" endpoint with the "local" provider."""
    endpoint = url_for("accounts.login_with_provider", provider="local")

    response = client.get(endpoint)
    check_view_response(response, status_code=302)

    with client:
        # The current user is only available here because of the dummy request context.
        # Otherwise, a request would need to be sent first.
        assert not current_user.is_authenticated

        username = password = dummy_user.identity.username
        response = client.post(
            endpoint, data={"username": username, "password": password}
        )

        check_view_response(response, status_code=302)
        assert current_user.is_authenticated


def test_login_with_provider_ldap(client, request_context):
    """Test the "accounts.login_with_provider" endpoint with the "ldap" provider."""
    endpoint = url_for("accounts.login_with_provider", provider="ldap")

    response = client.get(endpoint)
    check_view_response(response, status_code=302)

    with client:
        assert not current_user.is_authenticated

        # The actual data posted here does not matter (as long as it is not empty), as
        # the provider is mocked.
        response = client.post(endpoint, data={"username": "test", "password": "test"})

        check_view_response(response, status_code=302)
        assert current_user.is_authenticated


def test_login_with_provider_shib(client, request_context):
    """Test the "accounts.login_with_provider" endpoint with the "shib" provider."""
    idp = current_app.config["AUTH_PROVIDERS"]["shib"]["idps"][0]["entity_id"]
    endpoint = url_for("accounts.login_with_provider", provider="shib")

    response = client.post(endpoint, data={"idp": idp})

    check_view_response(response, status_code=302)
    assert idp in response.location

    with client:
        assert not current_user.is_authenticated

        response = client.get(endpoint)

        check_view_response(response, status_code=302)
        assert current_user.is_authenticated


def test_register(client):
    """Test the "accounts.register" endpoint."""
    response = client.post(
        url_for("accounts.register"),
        data={
            "username": "test",
            "displayname": "test",
            "email": "test@example.com",
            "password": "test1234",
            "password2": "test1234",
        },
    )

    check_view_response(response, status_code=302)
    assert LocalIdentity.query.filter_by(username="test").first() is not None


def test_request_email_confirmation(monkeypatch, client, user_session):
    """Test the "accounts.request_email_confirmation" endpoint."""
    monkeypatch.setitem(
        current_app.config["AUTH_PROVIDERS"]["local"],
        "email_confirmation_required",
        True,
    )

    endpoint = url_for("accounts.request_email_confirmation")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint)

        check_view_response(response, status_code=302)
        assert response.location == endpoint


def test_confirm_email(monkeypatch, client, dummy_user):
    """Test the "accounts.confirm_email" endpoint."""
    monkeypatch.setitem(
        current_app.config["AUTH_PROVIDERS"]["local"],
        "email_confirmation_required",
        True,
    )

    email = "test@example.com"
    token = dummy_user.identity.get_email_confirmation_token(email=email)

    assert not dummy_user.email_confirmed

    client.get(url_for("accounts.confirm_email", token=token))

    assert dummy_user.email_confirmed
    assert dummy_user.identity.email == email


def test_request_password_reset(client, dummy_user):
    """Test the "accounts.request_password_reset" endpoint."""
    endpoint = url_for("accounts.request_password_reset")

    response = client.get(endpoint)
    check_view_response(response)

    with client:
        response = client.post(
            endpoint, data={"username": dummy_user.identity.username}
        )

        check_view_response(response, status_code=302)
        # Just checking for the redirection is not enough here.
        assert "A password reset email has been sent." in get_flashed_messages()


def test_reset_password(client, dummy_user):
    """Test the "accounts.reset_password" endpoint."""
    token = dummy_user.identity.get_password_reset_token()

    response = client.get(url_for("accounts.reset_password", token=token))
    check_view_response(response)

    response = client.post(
        url_for("accounts.reset_password", token=token),
        data={"password": "test1234", "password2": "test1234"},
    )

    check_view_response(response, status_code=302)
    assert response.location == url_for("accounts.login")
    assert dummy_user.identity.check_password("test1234")


def test_logout(client, user_session):
    """Test the "accounts.logout" endpoint."""
    with user_session():
        assert current_user.is_authenticated

        client.get(url_for("accounts.logout"))

        assert not current_user.is_authenticated


def test_users(client, user_session):
    """Test the "accounts.users" endpoint."""
    with user_session():
        response = client.get(url_for("accounts.users"))
        check_view_response(response)


def test_view_user(client, dummy_user, user_session):
    """Test the "accounts.view_user" endpoint."""
    with user_session():
        response = client.get(url_for("accounts.view_user", id=dummy_user.id))
        check_view_response(response)


def test_view_resources(client, dummy_user, user_session):
    """Test the "accounts.view_resources" endpoint."""
    with user_session():
        response = client.get(url_for("accounts.view_resources", id=dummy_user.id))
        check_view_response(response)
