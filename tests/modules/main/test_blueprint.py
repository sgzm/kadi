# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app

from kadi.lib.web import make_next_url
from kadi.lib.web import url_for
from tests.utils import check_api_response
from tests.utils import check_view_response
from tests.utils import get_cookie


def test_app_errorhandler_api(api_client, client, dummy_access_token):
    """Test if the error handler of the app works correctly for API requests."""
    endpoint = "/api/404"

    # Unauthenticated request.
    response = api_client().get(endpoint)
    check_api_response(response, status_code=401)

    # Authenticated request.
    response = api_client(dummy_access_token).get(endpoint)
    check_api_response(response, status_code=404)
    # Check for any of the talisman headers.
    assert "Content-Security-Policy" in response.headers

    endpoint = url_for("api.new_record")

    # Unauthenticated request.
    response = api_client().put(endpoint)
    check_api_response(response, status_code=401)

    # Authenticated request.
    response = api_client(dummy_access_token).put(endpoint)
    check_api_response(response, status_code=405)
    # Check for any of the talisman headers.
    assert "Content-Security-Policy" in response.headers


def test_app_errorhandler_views(api_client, client, user_session):
    """Test if the error handler of the app works correctly for non-API requests."""
    endpoint = "/404"

    # Unauthenticated request.
    response = client.get(endpoint)

    check_view_response(response, status_code=302)
    assert response.location == make_next_url(endpoint)

    # Authenticated request.
    with user_session():
        response = client.get(endpoint)

        check_view_response(response, status_code=404)
        # Check for any of the talisman headers.
        assert "Content-Security-Policy" in response.headers

    endpoint = url_for("records.new_record")

    # Unauthenticated request.
    response = client.put(endpoint)

    check_view_response(response, status_code=302)
    assert response.location == make_next_url(endpoint)

    # Authenticated request.
    with user_session():
        response = client.put(endpoint)

        check_view_response(response, status_code=405)
        # Check for any of the talisman headers.
        assert "Content-Security-Policy" in response.headers


def test_after_app_request_locale_cookie(client):
    """Test if the locale cookie is set correctly."""
    for locale in current_app.config["LOCALES"]:
        client.cookie_jar.clear()

        client.get("/")
        cookie = get_cookie(client, "locale")

        assert cookie is not None
        assert cookie.value == current_app.config["LOCALE_DEFAULT"]

        # Using the request argument.
        client.get("/?locale=" + locale)
        cookie = get_cookie(client, "locale")

        assert cookie is not None
        assert cookie.value == locale

        # Using the cookie.
        client.get("/")
        cookie = get_cookie(client, "locale")

        assert cookie is not None
        assert cookie.value == locale
