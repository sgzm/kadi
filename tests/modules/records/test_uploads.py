# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import hashlib
import os
from io import BytesIO

import pytest
from flask import current_app
from sqlalchemy.exc import IntegrityError

from kadi.lib.storage.core import create_filepath
from kadi.modules.records.models import Upload
from kadi.modules.records.uploads import delete_upload
from kadi.modules.records.uploads import merge_chunks
from kadi.modules.records.uploads import remove_uploads
from kadi.modules.records.uploads import save_chunk


def test_delete_upload(dummy_upload):
    """Test if deleting uploads works correctly."""
    delete_upload(dummy_upload)
    assert dummy_upload.state == "inactive"


def test_remove_uploads(monkeypatch, tmp_path, new_upload):
    """Test if removing uploads works correctly."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    file_data = 10 * b"x"

    upload = new_upload(size=len(file_data))
    save_chunk(
        upload=upload, file_object=BytesIO(file_data), index=0, size=len(file_data)
    )

    remove_uploads(upload)

    assert not Upload.query.all()
    assert not os.listdir(tmp_path)


def test_save_chunk(monkeypatch, tmp_path, new_upload):
    """Test if saving chunks works correctly."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    file_data = 10 * b"x"
    index = 0

    upload = new_upload(size=len(file_data))
    save_chunk(
        upload=upload, file_object=BytesIO(file_data), index=index, size=len(file_data)
    )
    identifier = str(upload.id)

    assert os.listdir(tmp_path)[0] == identifier[:2]
    assert os.path.isfile(create_filepath(f"{identifier}-{index}"))


def test_merge_chunks_success(monkeypatch, tmp_path, dummy_record, new_upload):
    """Test if merging chunk data works correctly."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    file_data = 10 * b"x"
    prev_timestamp = dummy_record.last_modified

    upload = new_upload(
        size=len(file_data), checksum=hashlib.md5(file_data).hexdigest()
    )
    save_chunk(
        upload=upload, file_object=BytesIO(file_data), index=0, size=len(file_data)
    )
    file = merge_chunks(upload)

    assert upload.state == "inactive"
    assert file.state == "active"
    assert file.revisions.count() == 1
    assert dummy_record.last_modified > prev_timestamp
    assert str(file.id)[:2] in os.listdir(tmp_path)

    # Replace the previous file.
    file_data = 20 * b"x"
    prev_timestamp = dummy_record.last_modified
    prev_file_id = file.id

    upload = new_upload(
        size=len(file_data), checksum=hashlib.md5(file_data).hexdigest(), file=file
    )
    save_chunk(
        upload=upload, file_object=BytesIO(file_data), index=0, size=len(file_data)
    )
    file = merge_chunks(upload)

    assert upload.state == "inactive"
    assert file.state == "active"
    assert file.revisions.count() == 2
    assert file.id == prev_file_id
    assert dummy_record.last_modified > prev_timestamp
    assert str(file.id)[:2] in os.listdir(tmp_path)


def test_merge_chunks_error(db, dummy_file, new_upload):
    """Test if merging erroneous chunk data works correctly."""
    upload = new_upload()
    upload.name = dummy_file.name
    save_chunk(upload=upload, file_object=BytesIO(b""), index=0, size=0)

    # Start a new savepoint, so only the latest changes get rolled back.
    db.session.begin_nested()
    with pytest.raises(IntegrityError):
        merge_chunks(upload)

    assert upload.state == "inactive"
    assert dummy_file.state == "active"
    assert dummy_file.revisions.count() == 1
