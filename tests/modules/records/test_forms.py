# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from werkzeug.datastructures import MultiDict

from kadi.lib.resources.utils import add_link
from kadi.modules.records.forms import EditFileForm
from kadi.modules.records.forms import NewRecordForm
from kadi.modules.records.models import File


def test_new_record_form_template(
    dummy_template, dummy_user, new_collection, new_template, new_user
):
    """Test if prefilling a "NewRecordForm" with a template works correctly."""
    template_data = {"type": "test", "tags": ["test"]}
    template = new_template(data=template_data)
    form = NewRecordForm(template=template, user=dummy_user)

    type = template.data["type"]
    assert form.type.initial == (type, type)

    tag = template.data["tags"][0]
    assert form.tags.initial == [(tag, tag)]

    template = new_template(data=template_data, creator=new_user())
    form = NewRecordForm(template=template, user=dummy_user)

    assert form.type.initial is None
    assert form.tags.initial == []


def test_new_record_form_record(
    dummy_collection, dummy_record, dummy_user, new_record, new_user
):
    """Test if prefilling a "NewRecordForm" with a record works correctly."""
    dummy_record.set_tags(["test"])
    dummy_record.type = "test"
    add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    form = NewRecordForm(record=dummy_record, user=dummy_user)

    assert form.type.initial == (dummy_record.type, dummy_record.type)
    assert form.tags.initial == [("test", "test")]
    assert form.linked_collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]
    assert form.copy_permission.initial == (
        dummy_record.id,
        f"@{dummy_record.identifier}",
    )

    record = new_record(creator=new_user())
    form = NewRecordForm(record=record, user=dummy_user)

    assert form.type.initial is None
    assert form.tags.initial == []
    assert form.linked_collections.initial == []
    assert form.copy_permission.initial is None


def test_new_record_form_collection(
    dummy_collection, dummy_user, new_collection, new_user
):
    """Test if prefilling a "NewRecordForm" with a collection works correctly."""
    form = NewRecordForm(collection=dummy_collection, user=dummy_user)

    assert form.linked_collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]

    collection = new_collection(creator=new_user())
    form = NewRecordForm(collection=collection, user=dummy_user)

    assert form.linked_collections.initial == []


def test_file_form_validate_name(dummy_record, dummy_user):
    """Test if checking for duplicate files in the "EditFileForm" works correctly."""
    file = File.create(
        creator=dummy_user, record=dummy_record, name="test", size=0, state="active"
    )
    File.create(
        creator=dummy_user, record=dummy_record, name="test2", size=0, state="active"
    )

    form = EditFileForm(file=file, formdata=MultiDict({"name": "test"}))

    assert form.validate()

    form = EditFileForm(file=file, formdata=MultiDict({"name": "test2"}))

    assert not form.validate()
    assert "Name is already in use." in form.errors["name"]
