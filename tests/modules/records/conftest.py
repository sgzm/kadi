# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.tasks.models import Task
from kadi.modules.records.core import purge_record
from kadi.modules.records.files import package_files
from kadi.modules.records.uploads import merge_chunks


@pytest.fixture(autouse=True)
def _purge_record_task(monkeypatch):
    """Fixture to patch the task "kadi.records.purge_record".

    Will simulate the code the actual Celery task would run. The function to lanch the
    task will be patched in all relevant modules where it is used.

    Executed automatically for each test.
    """

    def _start_purge_record_task(record):
        purge_record(record)
        return True

    monkeypatch.setattr(
        "kadi.modules.records.api.internal.post.start_purge_record_task",
        _start_purge_record_task,
    )


@pytest.fixture(autouse=True)
def _merge_chunks_task(monkeypatch):
    """Fixture to patch the task "kadi.records.merge_chunks".

    Will simulate the code the actual Celery task would run. The function to lanch the
    task will be patched in all relevant modules where it is used.

    Executed automatically for each test.
    """

    def _start_merge_chunks_task(upload, user):
        task = Task.create(
            name="kadi.records.merge_chunks",
            creator=user,
            args=[str(upload.id)],
            state="running",
        )

        file = merge_chunks(upload)

        task.state = "success"
        task.result = {"file": str(file.id)}

        return task

    monkeypatch.setattr(
        "kadi.modules.records.api.v1_0.post.start_merge_chunks_task",
        _start_merge_chunks_task,
    )


@pytest.fixture
def package_files_task(monkeypatch, dummy_file):
    """Fixture to patch the task "kadi.records.package_files".

    Will simulate the code the actual Celery task would run. The function to lanch the
    task will be patched in all relevant modules where it is used.
    """

    def _start_package_files_task(record, user):
        task = Task.create(
            name="kadi.records.package_files",
            creator=user,
            args=[record.id],
            state="running",
        )

        temporary_file = package_files(record, user)

        task.state = "success"
        task.result = {"temporary_file_id": str(temporary_file.id)}

        return task

    monkeypatch.setattr(
        "kadi.modules.records.api.internal.post.start_package_files_task",
        _start_package_files_task,
    )
