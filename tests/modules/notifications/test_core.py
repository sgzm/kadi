# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.modules.notifications.core import create_notification_data
from kadi.modules.notifications.core import dismiss_notification
from kadi.modules.notifications.models import Notification


def test_create_notification_data(dummy_user):
    """Test if creating notification data works correctly."""
    notification_name = "test"

    notification = Notification.create(user=dummy_user, name=notification_name)
    title, body = create_notification_data(notification)

    assert title == body == notification_name


def test_dismiss_notification(db, dummy_user):
    """Test if dismissing notifications works correctly."""
    notification_name = "test"

    notification = Notification.create(user=dummy_user, name=notification_name)
    db.session.commit()
    dismiss_notification(notification)

    assert Notification.query.filter_by(name=notification_name).first() is None
