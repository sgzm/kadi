# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from kadi.modules.groups.core import delete_group
from kadi.modules.groups.models import Group
from tests.modules.resources import check_api_post_subject_resource_role
from tests.utils import check_api_response


def test_restore_group(client, db, dummy_group, user_session):
    """Test the internal "api.restore_group" endpoint."""
    with user_session():
        delete_group(dummy_group)
        response = client.post(url_for("api.restore_group", id=dummy_group.id))

        check_api_response(response)
        assert dummy_group.state == "active"


def test_purge_group(client, db, dummy_group, user_session):
    """Test the internal "api.purge_group" endpoint."""
    with user_session():
        delete_group(dummy_group)
        response = client.post(url_for("api.purge_group", id=dummy_group.id))

        check_api_response(response, status_code=204)
        assert Group.query.get(dummy_group.id) is None


def test_new_group(api_client, dummy_access_token):
    """Test the "api.new_group" endpoint."""
    response = api_client(dummy_access_token).post(
        url_for("api.new_group"), json={"identifier": "test", "title": "test"}
    )

    check_api_response(response, status_code=201)
    assert Group.query.filter_by(identifier="test").first() is not None


def test_add_group_member(api_client, dummy_access_token, dummy_group, new_user):
    """Test the "api.add_group_member" endpoint."""
    check_api_post_subject_resource_role(
        api_client(dummy_access_token),
        url_for("api.add_group_member", id=dummy_group.id),
        new_user(),
        dummy_group,
    )
