# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from marshmallow import ValidationError
from werkzeug.exceptions import HTTPException

from kadi.lib.conversion import lower
from kadi.lib.conversion import strip
from kadi.lib.schemas import check_duplicate_identifier
from kadi.lib.schemas import NonEmptyString
from kadi.lib.schemas import SortedPluck
from kadi.lib.tags.schemas import TagSchema
from kadi.modules.records.models import Record
from kadi.modules.records.schemas import RecordSchema


def test_kadi_schema(request_context):
    """Test if the custom schema base class works correctly."""
    schema = RecordSchema(only=["identifier"])

    assert not schema._internal

    schema.load_or_400({"identifier": "test"})

    with pytest.raises(HTTPException):
        schema.load_or_400(data=None)

    with pytest.raises(HTTPException):
        schema.load_or_400(data={"title": "test"})


def test_non_empty_string():
    """Test if the custom "NonEmptyString" fields works correctly."""
    assert NonEmptyString().deserialize("test") == "test"
    assert NonEmptyString().deserialize(" test ") == " test "
    assert NonEmptyString(filters=[lower, strip]).deserialize(" Test ") == "test"

    with pytest.raises(ValidationError):
        NonEmptyString().deserialize(" ")


def test_sorted_pluck():
    """Test if the custom "SortedPluck" fields works correctly."""
    assert (
        SortedPluck(TagSchema, "name")._serialize({"name": "test"}, "name", None)
        == "test"
    )
    assert SortedPluck(TagSchema, "name", many=True)._serialize(
        [{"name": "b"}, {"name": "a"}, {"name": "c"}], "name", None
    ) == ["a", "b", "c"]


def test_check_duplicate_identifier(dummy_record):
    """Test if checking for a duplicate identifier works correctly."""
    check_duplicate_identifier({"title": "test"}, Record)
    check_duplicate_identifier({"identifier": "test"}, Record)
    check_duplicate_identifier(
        {"identifier": dummy_record.identifier}, Record, exclude=dummy_record
    )

    with pytest.raises(ValidationError):
        check_duplicate_identifier({"identifier": dummy_record.identifier}, Record)
