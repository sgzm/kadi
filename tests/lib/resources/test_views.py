# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.views import add_links
from kadi.lib.resources.views import add_roles
from kadi.lib.resources.views import copy_roles
from kadi.lib.resources.views import remove_roles
from kadi.modules.accounts.models import User
from kadi.modules.collections.models import Collection
from kadi.modules.groups.models import Group
from kadi.modules.permissions.utils import add_role


def test_add_links(dummy_record, dummy_user, new_collection, new_user):
    """Test if adding links in view functions works correctly."""

    # Add some valid, newly created collections.
    valid_collection_ids = [new_collection().id for _ in range(0, 3)]

    # Add one collection without permission and one non-existent one.
    invalid_collection_ids = []
    invalid_collection_ids.append(new_collection(creator=new_user()).id)
    invalid_collection_ids.append(invalid_collection_ids[-1] + 1)

    add_links(
        Collection,
        dummy_record.collections,
        valid_collection_ids + invalid_collection_ids,
        user=dummy_user,
    )

    collection_ids = [
        c[0] for c in dummy_record.collections.with_entities(Collection.id).all()
    ]

    # Check if all valid collections were added.
    for valid_collection_id in valid_collection_ids:
        assert valid_collection_id in collection_ids

    # Check if the invalid collections were not added.
    for invalid_collection_id in invalid_collection_ids:
        assert invalid_collection_id not in collection_ids


def test_add_roles(dummy_record, new_group, new_user):
    """Test if adding roles in view functions works correctly."""
    users = [new_user() for _ in range(0, 3)]

    # Include one invalid user ID.
    user_ids = [user.id for user in users] + [users[-1].id + 1]
    add_roles(User, user_ids, dummy_record, "member")

    for user in users:
        assert user.roles.filter_by(object="record").one().name == "member"

    groups = [new_group() for _ in range(0, 3)]

    # Include one invalid group ID.
    group_ids = [group.id for group in groups] + [groups[-1].id + 1]
    add_roles(Group, group_ids, dummy_record, "member")

    for group in groups:
        assert group.roles.filter_by(object="record").one().name == "member"


def test_remove_roles(dummy_record, dummy_user, new_group, new_user):
    """Test if removing roles in view functions works correctly."""
    users = [new_user() for _ in range(0, 3)]

    # Include one invalid user ID and the creator's user ID.
    user_ids = [user.id for user in users] + [users[-1].id + 1, dummy_user.id]
    add_roles(User, user_ids, dummy_record, "member")

    remove_roles(User, user_ids, dummy_record)

    for user in users:
        assert not user.roles.filter_by(object="record").all()

    assert dummy_user.roles.filter_by(object="record").one().name == "admin"

    groups = [new_group() for _ in range(0, 3)]

    # Include one invalid group ID.
    group_ids = [group.id for group in groups] + [groups[-1].id + 1]
    add_roles(Group, group_ids, dummy_record, "member")

    remove_roles(Group, group_ids, dummy_record)

    for group in groups:
        assert not group.roles.filter_by(object="record").all()


def test_copy_roles(dummy_group, dummy_record, dummy_user, new_record, new_user):
    """Test if copying roles in view functions works correctly."""
    user = new_user()
    record = new_record(creator=user)

    user_role_query = dummy_user.roles.filter_by(object="record", object_id=record.id)
    group_role_query = dummy_group.roles.filter_by(object="record", object_id=record.id)

    copy_roles(record, None)

    assert not user_role_query.all()
    assert not group_role_query.all()

    copy_roles(record, dummy_record.id)

    assert not user_role_query.all()
    assert not group_role_query.all()

    # Give the user permission to read the record to copy the roles from.
    add_role(user, "record", dummy_record.id, "member")
    # Give the group a permission that should be copied over.
    add_role(dummy_group, "record", dummy_record.id, "member")

    copy_roles(record, dummy_record.id)

    user_role = user_role_query.first()
    assert user_role is not None and user_role.name == "admin"
    group_role = group_role_query.first()
    assert group_role is not None and group_role.name == "member"
