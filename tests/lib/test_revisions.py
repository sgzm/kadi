# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.revisions.core import create_revision
from kadi.lib.revisions.models import Revision
from kadi.lib.revisions.schemas import ObjectRevisionSchema
from kadi.lib.revisions.utils import get_revision_columns
from kadi.modules.records.core import update_record
from kadi.modules.records.models import Record
from kadi.modules.records.schemas import RecordSchema


def test_create_revision(dummy_record, dummy_user):
    """Test if revisions are created correctly."""
    first_revision = dummy_record.revisions[0]

    assert dummy_record.revisions.count() == 1
    assert first_revision._model_class == dummy_record.__class__
    assert first_revision.record_id == dummy_record.id
    assert first_revision.revision.user_id == dummy_user.id
    assert first_revision.parent is None

    create_revision(dummy_record, user=dummy_user)

    assert dummy_record.revisions.count() == 1

    dummy_record.identifier = "test"
    dummy_record.set_tags(["test"])
    create_revision(dummy_record, user=dummy_user)
    second_revision = dummy_record.revisions[1]

    assert dummy_record.revisions.count() == 2
    assert first_revision.revision.user_id == dummy_user.id
    assert second_revision.parent.id == first_revision.id
    assert second_revision.identifier == "test"
    assert second_revision.tags == [{"name": "test"}]


def test_object_revision_schema(dummy_record, dummy_user, user_context):
    """Test if the object revision schema works correctly."""
    with user_context():
        update_record(dummy_record, tags=["test"])

    revision = dummy_record.revisions.join(Revision).order_by(Revision.timestamp)[-1]

    schema = ObjectRevisionSchema(
        schema=RecordSchema,
        api_endpoint="api.get_record_revision",
        view_endpoint="records.view_record_revision",
        endpoint_args={"record_id": dummy_record.id},
    )
    data = schema.dump(revision)

    assert data["object_id"] == dummy_record.id
    assert "test" in data["data"]["tags"]
    assert data["diff"] == {"tags": {"new": ["test"], "prev": []}}


def test_get_revision_columns(monkeypatch):
    """Test if revision columns are parsed correctly."""
    monkeypatch.setattr(Record.Meta, "revision", ["test", "foo[ bar, baz ]"])

    assert get_revision_columns(Record) == (["test"], [("foo", ["bar", "baz"])])
