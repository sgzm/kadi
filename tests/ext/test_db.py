# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from werkzeug.exceptions import NotFound

from kadi.modules.records.core import delete_record
from kadi.modules.records.models import Record


def test_query_get_active_or_404(dummy_record, user_context):
    """Test if the "get_active_or_404" query shortcut works correctly."""
    with user_context():
        assert Record.query.get_active_or_404(dummy_record.id).id == dummy_record.id

        delete_record(dummy_record)

        with pytest.raises(NotFound):
            Record.query.get_active_or_404(dummy_record.id)


def test_query_active(dummy_record, user_context):
    """Test if the "active" query shortcut works correctly."""
    with user_context():
        assert Record.query.active().first().id == dummy_record.id

        delete_record(dummy_record)

        assert Record.query.active().first() is None
