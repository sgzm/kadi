# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app
from flask_wtf.csrf import generate_csrf

from kadi.lib.web import url_for
from tests.utils import check_api_response
from tests.utils import check_view_response


def test_csrf_session(monkeypatch, client, user_session):
    """Test if CSRF protection using the session works correctly."""
    with user_session():
        monkeypatch.setitem(current_app.config, "WTF_CSRF_ENABLED", True)

        csrf_message = b"The CSRF token is missing."

        response = client.post(url_for("records.new_record"))

        check_view_response(response, status_code=400)
        assert csrf_message in response.data

        response = client.post(
            url_for("records.new_record"), data={"csrf_token": generate_csrf()}
        )

        check_view_response(response)
        assert csrf_message not in response.data


def test_csrf_api(monkeypatch, api_client, client, dummy_access_token, user_session):
    """Test if CSRF protection using the API works correctly."""

    # Test the API using an access token.
    with monkeypatch.context() as m:
        m.setitem(current_app.config, "WTF_CSRF_ENABLED", True)

        response = api_client(dummy_access_token).post(
            url_for("api.new_record"), json={"identifier": "test", "title": "test"}
        )
        check_api_response(response, status_code=201)

    # Test the API using the session (see kadi/ext/login.py).
    with user_session():
        monkeypatch.setitem(current_app.config, "WTF_CSRF_ENABLED", True)

        # Set the session cookie.
        client.get("/")

        response = client.post(
            url_for("api.new_record"), json={"identifier": "test2", "title": "test"}
        )

        check_api_response(response, status_code=400)
        assert response.get_json()["description"] == "The CSRF token is missing."

        response = client.post(
            url_for("api.new_record"),
            json={"identifier": "test2", "title": "test"},
            headers={"X-CSRF-TOKEN": generate_csrf()},
        )
        check_api_response(response, status_code=201)
