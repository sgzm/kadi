# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.utils import utcnow
from kadi.lib.web import make_next_url
from kadi.lib.web import url_for
from tests.utils import check_api_response
from tests.utils import check_view_response


def test_load_user_from_session(client, user_session):
    """Test if loading a user from the session works correctly."""
    with user_session():
        # Test a non-API endpoint.
        response = client.get(url_for("records.records"))
        check_view_response(response)

        # Test an API endpoint.
        response = client.get(url_for("api.index"))
        check_api_response(response)


def test_load_user_from_request(api_client, dummy_access_token, new_access_token):
    """Test if loading a user from a request works correctly."""

    # Test an API endpoint without a token.
    response = api_client().get(url_for("api.index"))
    check_api_response(response, status_code=401)

    # Test an API endpoint with an invalid token.
    response = api_client("test").get(url_for("api.index"))
    check_api_response(response, status_code=401)

    # Test a non-API endpoint with a valid token.
    response = api_client(dummy_access_token).get(url_for("main.index"))
    check_api_response(response, status_code=404)

    # Test an API endpoint with a valid token.
    response = api_client(dummy_access_token).get(url_for("api.index"))
    check_api_response(response)

    # Test an API endpoint with an expired token.
    expired_token = new_access_token(expires_at=utcnow())
    response = api_client(expired_token).get(url_for("api.index"))

    check_api_response(response, status_code=401)
    assert "Access token has expired." in response.get_json()["description"]


def test_unauthorized(api_client, client, dummy_user, user_session):
    """Test if unauthorized requests are handled correctly."""

    # Test an API request.
    response = api_client().get(url_for("api.index"))

    check_api_response(response, status_code=401)
    assert "No valid access token was supplied." in response.get_json()["description"]

    # Test a non-API request.
    endpoint = url_for("records.records")
    response = client.get(endpoint)

    check_view_response(response, status_code=302)
    assert response.location == make_next_url(endpoint)
